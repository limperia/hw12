<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/Array.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Technics.php';



try {
    $sql = "SELECT * FROM technics";
    $answerObject =  $connection->query($sql);
    $array_technics = $answerObject->fetchAll();
    $technicsObject = [];
    foreach ($array_technics as $arr_technic) {
        $technicsObject[] = new Technic($arr_technic['id'], $arr_technic['title'], $arr_technic['price'], $arr_technic['description'], $arr_technic['type']);
                
    }
} catch (Exception $exe_error) {
    die('Oшибка получения technics!!!!<br>' . $exe_error->getMessage());
}


?>

<?php include $_SERVER['DOCUMENT_ROOT'] .'/header.php'; ?>

<section id="tables_main">
    <div class="containar-fluid">
        <div class="container">
        <div class="row">
        <?php if(!empty($_GET['notification'])):?>
            <?php if($_GET['notification'] == 'entry_saved'):?>
                <div class="alert alert-success" role="alert">
                    Note has been saved succesfully!
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>
            <div class="row">

            <?php foreach ($technicsObject as  $techObjects) : ?>
           
            <?php $techObjects->getFile('list'); ?> 
            
                <?php endforeach; ?>
            </div>
        </div>
</section>



<?php include $_SERVER['DOCUMENT_ROOT'] .'/footer.php'; ?>

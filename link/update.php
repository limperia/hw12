<?php
if(empty($_POST['type']) || empty($_POST['title']) || empty($_POST['price']) || empty($_POST['description']) || empty($_POST['id'])){
    header('Location:/');
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Technics.php';


$id = $_POST['id'];
$title = $_POST['title'];
$price = $_POST['price'];
$description = $_POST['description'];
$type = $_POST['type'];

$techObjects = new Technic($id, $title, $price, $description, $type);
$techObjects -> update($connection);
header('Location:/?notification=updated');
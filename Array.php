<?php

$array_technics = [
    [
        'title' => 'Xiaomi Mi 11 Lite 5G 8/128GB Truffle Black', 
        'price' => 11200,
        'description' => 'Смартфон • 2 или 1 + карта памяти SIM • экран: 6,55" • AMOLED • 2400х1080 • встроенная память: 128 ГБ • оперативная память: 8 ГБ • процессор: Qualcomm Snapdragon 780G + Adreno 642 • ОС: Android 11 • аккумулятор: 4250 мАч (несъемная) • камера: 64 (f/1.79) + 8 (f/2.2) + 5 (f/2.4) Мп • цвет: черный • NFC: + • 03.2021 • Модель из линейки Mi 11 Lite 5G',
        'type' => 'phone'
        

    ],
    [
        'title' => 'HUAWEI P40 8/128GB Black (51095EHY)', 
        'price' => 17600,
        'description' => 'Смартфон • 2 или 1 + карта памяти SIM • экран: 6,1" • OLED • 2340x1080 • встроенная память: 128 ГБ • оперативная память: 8 ГБ • процессор: Huawei Kirin 990 • ОС: Android 10 • аккумулятор: 3800 мАч (несъемный) • камера: 50 (широкоугольная камера, f/1.9) + 16 ... • цвет: черный • NFC: + • 03.2020 • Модель из линейки P40',
        'type' => 'phone'
    

    ],
    [
        'title' => 'Samsung Galaxy A52 8/256GB Black (SM-A525FZKI)', 
        'price' => 11800,
        'description' => 'Смартфон • 2 или 1 + карта памяти SIM • экран: 6,5" • Super AMOLED • 2400x1080 • встроенная память: 256 ГБ • оперативная память: 8 ГБ • процессор: Qualcomm Snapdragon 720G • ОС: Android 11 • аккумулятор: 4500 мАч (несъемный) • камера: 64 (f/1.8) + 12 (f/2.2, ... • цвет: черный • NFC: + • 03.2021 • Модель из линейки Galaxy A52 ',
        'type' => 'phone'
       
    ],
    [
        'title' => 'Apple iPhone 12 Pro 128GB Pacific Blue (MGMN3/MGLR3)', 
        'price' => 32500,
        'description' => 'Смартфон • 2 SIM • экран: 6,1" • OLED • 1170x2532 • встроенная память: 128 ГБ • оперативная память: 6 ГБ • процессор: Apple A14 Bionic • ОС: Apple iOS 14 • аккумулятор: 2815 мАч (несъемный) • камера: 12 (f/1.6, широкоугольная) + 12 (f/2.4, ... • цвет: тихоокеанский синий • NFC: + (Apple Pay) • 10.2020 • Модель из линейки iPhone 12 Pro ',
        'type' => 'phone'
      

    ],
    [
        'title' => 'HUAWEI P smart 2021 4/128GB Crush Green (51096ABX)', 
        'price' => 4800,
        'description' => 'Смартфон • 2 SIM • экран: 6,67" • IPS • 1080x2400 • встроенная память: 128 ГБ • оперативная память: 4 ГБ • процессор: HiSilicon Kirin 710A + Mali-G51 MP4 • ОС: Android 10 (Emotion UI 10.1) • аккумулятор: 5000 мАч (несъемный) • камера: 48 (широкоуголная f/1.8) + 8 (ультраширокоугольная) + 2 (макро) + 2 (глубина) Мп • цвет: зеленый ',
        'type' => 'phone'
        

    ],
    [
        'title' => ' GARMIN', 
        'price' => 12400,
        'description' => 'GPS; Пульсометр; Шагомер; Опция «Найти устройство»; Календарь; GPRS; Будильник; Wi-Fi; Спортивное ПО; Акселерометр; Отслеживание уровня активности в течение дня; Датчик сердечного ритма; Темп мин/км; Индикатор разряда батареи; Датчик качества сна; Электронный компас; Альтиметр; Глонасс; Управление музыкой',
        'type' => 'watch'
        

    ],
    [
        'title' => ' GARMIN', 
        'price' => 7550,
        'description' => 'GPS; Пульсометр; Шагомер; Опция «Найти устройство»; Календарь; GPRS; Будильник; Wi-Fi; Спортивное ПО; Акселерометр; Отслеживание уровня активности в течение дня; Датчик сердечного ритма; Темп мин/км; Индикатор разряда батареи; Датчик качества сна; Электронный компас; Альтиметр; Глонасс; Управление музыкой',
        'type' => 'watch'
      

    ],
    
    [
        'title' => ' GARMIN', 
        'price' => 7100,
        'description' => 'GPS; Пульсометр; Шагомер; Опция «Найти устройство»; Календарь; Будильник; Спортивное ПО; Акселерометр; Отслеживание уровня активности в течение дня; Подсчет потраченных калорий; Датчик сердечного ритма; Интервальные таймеры; Темп мин/км; Память на 25 часов тренировок; Индикатор разряда батареи; Гироскоп; Барометр; Датчик вращения педалей; Продвинутые тренировки; Датчик качества сна; Электронный компас; Альтиметр; Информация о восходе и закате солнца и фазах луны; Измерение температуры; ',
        'type' => 'watch'

    ],
    [
        'title' => ' GARMIN', 
        'price' => 7550,
        'description' => 'GPS; Пульсометр; Шагомер; Опция «Найти устройство»; Календарь; GPRS; Будильник; Wi-Fi; Спортивное ПО; Акселерометр; Отслеживание уровня активности в течение дня; Датчик сердечного ритма; Темп мин/км; Индикатор разряда батареи; Датчик качества сна; Электронный компас; Альтиметр; Глонасс; Управление музыкой',
        'type' => 'watch'
  
    ],
    [
        'title' => ' Suunto', 
        'price' => 8250,
        'description' => 'Функции умных часов	GPS; Bluеtooth 4.0; Шагомер; Будильник; Подсчет потраченных калорий; Датчик сердечного ритма; Интервальные таймеры; Датчик качества сна; Информация о восходе и закате солнца и фазах луны',
        'type' => 'watch'
       

    ],
    [
        'title' => 'Jacques Lemans LM-1-1841G-K', 
        'price' => 6570,
        'description' => 'Женские часы. Часовые механизмы: Кварцевые часы. Материалы корпуса часов: Нержавеющая сталь с PVD покрытием желтого цвета. Стекло: Минеральное стекло
        Размеры корпуса: Диаметр 37 мм. Тип браслета: Браслет из нержавеющей стали с PVD покрытием желтого цвета. Водозащита: 100 метров.Корпус часов украшен кристаллами Swarovski ',
        'type' => 'watch'
     

    ],
    [
        'title' => 'Lenovo Legion 5 15ARH05H (82B1007VPB)', 
        'price' => 30600,
        'description' => 'Ноутбук • Классический • 15,6" • IPS • 1920x1080 • AMD Ryzen 5 4600H • 3,0 ГГц • ОЗУ: 8 ГБ • NVIDIA GeForce RTX 2060, 6 ГБ GDDR6 • SSD: 512 ГБ • 2.46 кг • ОС: DOS • цвет: черный',
        'type' => 'Laptop'
      
    ],
    [
        'title' => 'Apple MacBook Pro 13" Space Gray Late 2020 (MYD82)', 
        'price' => 40500,
        'description' => 'Ноутбук • Классический • 13,3" • IPS • 2560x1600 • Apple M1 • ОЗУ: 8  ГБ • 8 core GPU • SSD: 256 ГБ • 1,4 кг • ОС: macOS Big Sur • цвет: серый космос • 11.2020 • Модель из линейки MacBook Pro 13" Late 2020 ',
        'type' => 'Laptop'
      
    ],
    [
        'title' => 'HUAWEI MateBook X Pro 2020 Emerald Green (53010VUL)', 
        'price' => 46200,
        'description' => 'Ноутбук • Классический • 13,9" • LTPS • 3000x2000 • Intel Core i7-10510U • 1,8 ГГц • ОЗУ: 16 ГБ • NVIDIA GeForce MX 250, 2 ГБ GDDR5 • SSD: 1000 ГБ • 1,3 кг • ОС: Windows 10 Home • цвет: изумрудный ',
        'type' => 'Laptop'
       
    ]
];




?>


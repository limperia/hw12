



<div class="row d-flex justify-content-center">
    <div class="col-md-6">
        <h1>Редактирование товара:</h1>
        <form action="/link/update.php" method="post">
            <input type="hidden" name="id" value="<?=$this->id?>">
            <div class="form-group">
                <label> Название:
                    <input type="text" value="<?=$this->title?>" name="title" class="form-control" 
                    placeholder="Введите название">
                </label>
            </div>
            <div class="form-group">
                <label> Цена:
                    <input type="text" value="<?=$this->price?>" name="price" class="form-control" 
                    placeholder="Введите цену">
                </label>
            </div>
            <div class="form-group">
                <label> Описание:
                    <textarea name="description" placeholder="Введите описание" class="form-control"><?=$this->description?></textarea>
                </label>
            </div>
            <div class="form-group">
                <label> Тип товара:
                    <input type="text" value="<?=$this->type?>" name="type" class="form-control" 
                    placeholder="Введите тип товара">
                </label>
            </div>
            

            <button class="btn btn-primary">Обновить</button>
        </form>
    </div>
</div>

























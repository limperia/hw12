<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/Array.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/Technics.php';

try{

    if(empty($_GET['id'])){
        header('Location:/');
    }
    $id = (int)$_GET['id'];
    
    $techObjects = Technic::create($id, $connection);

}catch(Exception $exc_error){
    die('Ошибка получить technics !<br>'. $exc_error -> getMessage());
}

?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/header.php';?>


      <div class="container">
            <div class="row edit_stile1">

    <?php $techObjects -> getFile('edit_form'); ?> 

            </div>
      </div>

 <?php include_once $_SERVER['DOCUMENT_ROOT'] .'/footer.php'; ?>
 

